package main.rest.vote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/back/vote")
public class VoteController {

	@Autowired
	private VotoRepository votoRepository;

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Voto> getVotos() {
		return votoRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody VotoVO vote(@RequestBody Voto voto) {
		VotoVO retorno = new VotoVO();

		try {
			votoRepository.save(voto);
			retorno.setMsg("votado");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Error("Você já votou!");
		}

		return retorno;
	}

}
