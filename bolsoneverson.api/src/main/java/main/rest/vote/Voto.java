package main.rest.vote;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pessoa_voto")
public class Voto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	// unique = true - importante para as classes de teste
	// banco é criado em tempo de execução em teste
	@Column(unique = true)
	private String nome;

	@Column(name = "is_facista")
	private Boolean facista;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getFacista() {
		return facista;
	}

	public void setFacista(Boolean facista) {
		this.facista = facista;
	}

}
