package main.rest.votos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import main.rest.vote.Voto;
import main.rest.vote.VotoRepository;

@RestController
@RequestMapping(path = "/back/votos")
public class VotosController {
	
	@Autowired
	private VotoRepository votoRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public Integer[] getVotos() {
		Integer[] retorno = { 0, 0 };
		
		Iterable<Voto> votos = votoRepository.findAll();
		for (Voto voto : votos) {
			if (voto.getFacista()) {
				retorno[0] += 1;
			} else {
				retorno[1] += 1;
			}
		}
		
		return retorno;
	}

}
