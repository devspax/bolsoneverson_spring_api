package main.rest.ping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/back/ping")
public class PingController {
    
    @RequestMapping(method = RequestMethod.GET)
    public Pong greeting() {
        return new Pong("Pong");
    }
	
}
