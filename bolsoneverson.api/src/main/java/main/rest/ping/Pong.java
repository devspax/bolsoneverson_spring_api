package main.rest.ping;

public class Pong {

	private final long id;
	private final String content;
	private static int counter;
	
	public Pong(String content) {
		this.id = counter;
		this.content = content;
		counter++;
	}
	
	public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
	
}
