package ping;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import main.rest.ping.Pong;

public class TesteCounter {
	
	@Test
	public void testPongClassCounter() {
		String primeiroPong = "Primeiro Pong";
		String segundoPong = "Segundo Pong";
		
		Pong pong = new Pong(primeiroPong);
		Pong pong2 = new Pong(segundoPong);
		
		assertEquals("A classe deve ter inicializado com o id certo", 0, pong.getId());
		assertEquals("A classe deve ter inicializado com a mensagem certa", primeiroPong, pong.getContent());
		
		assertEquals("A classe deve ter inicializado com o id certo", 1, pong2.getId());
		assertEquals("A classe deve ter inicializado com a mensagem certa", segundoPong, pong2.getContent());
	}

}
