package main.rest.ping;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import main.rest.Starter;
import main.rest.utils.TestUtils;
import main.rest.vote.VoteController;
import main.rest.vote.Voto;
import main.rest.vote.VotoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Starter.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@ActiveProfiles("test")
public class VoteControllerTest {
	
	private String BASE_URL = "/back/vote";

	@Autowired
	private MockMvc mvc;

	@Mock
	private VotoRepository votoRepository;

	@InjectMocks
	private VoteController voteController;
	
	@Test
	public void shouldGetList() throws Exception {
		this.mvc
			.perform(get(BASE_URL)
			.accept(MediaType.APPLICATION_JSON))
			.andExpect(status()
			.isOk());
	}

	@Test
	public void shouldVote() throws Exception {
		Voto v = new Voto();
		v.setNome("Teste person");
		v.setFacista(false);
		
		this.mvc
			.perform(post(BASE_URL)
					.contentType(MediaType.APPLICATION_JSON)
					.content(TestUtils.transform(v))
			.accept(MediaType.APPLICATION_JSON))
			.andExpect(status()
			.isOk());
	}
	
	@Test
	public void shouldGenerateError() throws Exception {
		Voto v = new Voto();
		v.setNome("Teste person2");
		v.setFacista(false);
		
		this.mvc
			.perform(post(BASE_URL)
					.contentType(MediaType.APPLICATION_JSON)
					.content(TestUtils.transform(v))
			.accept(MediaType.APPLICATION_JSON))
			.andExpect(status()
			.isOk());
		
		this.mvc
			.perform(post(BASE_URL)
					.contentType(MediaType.APPLICATION_JSON)
					.content(TestUtils.transform(v))
			.accept(MediaType.APPLICATION_JSON))
			.andExpect(status()
			.is4xxClientError());
	}

}
